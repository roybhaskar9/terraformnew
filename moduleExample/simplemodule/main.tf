provider "local" {
}

module "child" {
  source = "./child"
  memory = "1G"

}
resource "local_file" "foo" {
    content     = "foo!"
    filename = "${path.module}/foo.bar"
}

output "child_memory" {
  value = "${module.child.received}"
}
