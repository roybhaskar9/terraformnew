variable "aws_access_key" {
  default     = "AWSACCESSKEYEXAMPLE"
  description = "AWS ACCESS KEY"
}

variable "aws_secret_key" {
  default     = "AWSSECRETKEYEXAMPLE"
  description = "AWS SECRET KEY"
}


variable "cluster_name" {
  default = "rds-sample-cluster"
} 
  
variable "instance_class" {
  default = "db.t2.small"
}

variable "username" {
  default = "master"
}

variable "password" {
  default = "password"
}
