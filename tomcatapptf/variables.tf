variable "access_key"{}
variable "secret_key"{}
variable "key_name"{
default="terraformvideo"
}
variable "key_path"{
default="terraformvideo.pem"
}
 
variable "aws_region"{
description="Region to Launch Server's"
default="us-east-1"
}
 
variable "aws_amis"{
default = {
us-east-1="ami-9eaa1cf6"
   }
}
