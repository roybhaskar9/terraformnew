provider "aws" {
  region = "us-east-1"
  #access_key = ""
  #secret_key = ""
}

resource "aws_instance" "example" {
  ami           = "ami-2757f631"
  instance_type = "t2.micro"
}

terraform {
  backend "s3" {
    bucket = "s9tfvideobucket"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}
